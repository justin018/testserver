from django.conf.urls import patterns, include, url
from django.contrib import admin
from myproject.hello import helloWorld, getMagicData

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myproject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'hello/', helloWorld),
    url(r'magic/', getMagicData),
)
